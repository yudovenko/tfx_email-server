### Ansible playbooks for Email server
Ansible playbooks for setup, provisioning and move configs from production Email server.
___
### How to use

* Edit ``` inventory ``` and change ip address and creds for current server
* ` ansible-playbook -i inventory site.yml ` - this will run playbooks for setup new email server
___
### Copy home directory with RSYNC
*  **Add your ssh key to /root/.ssh/known_hosts on new server**
* **run** `rsync -a -e "ssh  -i /home/%username%/.ssh/tfx" -arv /home/ root@example.com:/home/ `
Where:
``` %username% ```- name of user which consists ssh key for connect to new email server
Rsync will copy home directories of users, include mailboxes of the users.
___
#### Links
http://docs.ansible.com/
https://linux.die.net/man/1/rsync